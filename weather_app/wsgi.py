# -*- coding: utf-8 -*-

"""
.. module:: .wsgi.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from weather.app import create_app

app = create_app()
