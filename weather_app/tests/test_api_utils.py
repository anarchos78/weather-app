# -*- coding: utf-8 -*-

"""
.. module:: .test_api_utils.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import unittest

from unittest.mock import Mock, patch

from weather.app import create_app
from weather.utils import (
    fetch_weather_data, get_forecast, convert_dt_txt,
    convert_kelvin_to_celsius, format_date, format_time
)


class TestUtils(unittest.TestCase):
    """Test API helper functions"""

    def setUp(self):
        """Test setUp"""

        self.app = create_app()

        self.app.config['TESTING'] = True
        self.app.config['DEBUG'] = False

        self.client = self.app.test_client()

        self.external_data = {
            "cod": "200",
            "message": 0.0029,
            "cnt": 40,
            "list": [{
                "dt": 1524344400,
                "main": {
                    "temp": 289.82,
                    "temp_min": 289.82,
                    "temp_max": 292.929,
                    "pressure": 1022.52,
                    "sea_level": 1029.93,
                    "grnd_level": 1022.52,
                    "humidity": 53,
                    "temp_kf": -3.11
                },
                "weather": [{
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }],
                "clouds": {
                    "all": 92
                },
                "wind": {
                    "speed": 3.17,
                    "deg": 188.501
                },
                "rain": {
                    "3h": 0.00627
                },
                "sys": {
                    "pod": "n"
                },
                "dt_txt": "2018-04-21 21:00:00"
            }],
            "city": {
                "id": 2643743,
                "name": "London",
                "coord": {
                    "lat": 51.5085,
                    "lon": -0.1258
                },
                "country": "GB",
                "population": 1000000
            }
        }

        self.expected_forecast_celsius = {
            'description': 'light rain',
            'temperature': '17C',
            'pressure': 1022.52,
            'humidity': '53%'
        }

        self.expected_forecast_kelvin = {
            'description': 'light rain',
            'temperature': '289.82K',
            'pressure': 1022.52,
            'humidity': '53%'
        }

        self.expected_humidity = {'humidity': '53%'}

        self.expected_temp = {'temperature': '17C'}
        self.expected_temp_c = {'temperature': '17C'}
        self.expected_temp_k = {'temperature': '289.82K'}

    def tearDown(self):
        """Clean up"""

        pass

    @patch('weather.utils.requests.get')
    def test_fetch_weather_data(self, mock_get):
        """Tests fetch_weather_data function"""

        with self.app.test_request_context():
            # Configure the mock to return a response with an OK status code.
            mock_get.return_value = Mock(ok=True)
            mock_get.return_value.json.return_value = self.external_data

            response = fetch_weather_data()

            self.assertIsNotNone(response)
            self.assertEqual(response, self.external_data)

    @patch('weather.utils.requests.get')
    def test_get_forecast(self, mock_get):
        """Tests get_forecast function"""

        mock_get.return_value.json.return_value = self.external_data
        with self.app.test_request_context():
            forecast_celsius = get_forecast(
                date='20180421', time='2100', temp_unit='C'
            )

            forecast_kelvin = get_forecast(
                date='20180421', time='2100', temp_unit='K'
            )

            forecast_humidity = get_forecast(
                date='20180421', time='2100', humidity=None
            )

            forecast_temp = get_forecast(
                date='20180421', time='2100', temperature=None
            )

            forecast_temp_celsius = get_forecast(
                date='20180421', time='2100', temperature=None, temp_unit='C'
            )

            forecast_temp_kelvin = get_forecast(
                date='20180421', time='2100', temperature=None, temp_unit='K'
            )

        self.assertEqual(forecast_celsius, self.expected_forecast_celsius)
        self.assertEqual(forecast_kelvin, self.expected_forecast_kelvin)
        self.assertEqual(forecast_humidity, self.expected_humidity)
        self.assertEqual(forecast_temp, self.expected_temp)
        self.assertEqual(forecast_temp_celsius, self.expected_temp_c)
        self.assertEqual(forecast_temp_kelvin, self.expected_temp_k)

    def test_convert_dt_txt(self):
        """Tests convert_dt_txt function"""

        dt_txt_good = convert_dt_txt('2018-04-21 12:00:00')

        self.assertIsInstance(dt_txt_good, tuple)
        self.assertEqual(dt_txt_good,  ('20180421', '1200'))

    def test_convert_kelvin_to_celsius(self):
        """Tests convert_kelvin_to_celsius function"""

        k_to_c = convert_kelvin_to_celsius(289.82)

        self.assertIsInstance(k_to_c, int)
        self.assertEqual(k_to_c,  17)

    def test_format_date(self):
        """Tests format_date function"""

        good_date = format_date('20180421')
        bad_date_len_more = format_date('201804210')
        bad_date_len_less = format_date('2018042')

        self.assertIsInstance(good_date, str)
        self.assertEqual(good_date,  '2018-04-21')
        self.assertEqual(bad_date_len_more,  '201804210 (date not valid)')
        self.assertEqual(bad_date_len_less,  '2018042 (date not valid)')

    def test_format_time(self):
        """Tests format_time function"""

        good_time = format_time('2100')
        bad_time_len = format_time('210')
        bad_time_range = format_time('2400')

        self.assertIsInstance(good_time, str)
        self.assertEqual(good_time,  '21:00')
        self.assertEqual(bad_time_len,  '210 (time not valid)')
        self.assertEqual(bad_time_range,  '2400 (time not valid)')


if __name__ == "__main__":
    unittest.main()
