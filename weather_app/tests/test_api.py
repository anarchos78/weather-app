# -*- coding: utf-8 -*-

"""
.. module:: .test_api.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import json
import unittest

from time import localtime, strftime, time

from weather.app import create_app
from weather.utils import convert_dt_txt


class ApiTests(unittest.TestCase):
    """Tests the weather API"""

    def setUp(self):
        """Setup"""

        self.app = create_app()

        self.app.config['TESTING'] = True
        self.app.config['DEBUG'] = False

        self.client = self.app.test_client()

        self.date_time = convert_dt_txt(
            strftime("%Y-%m-%d %H:%M:%S", localtime(time() + 24 * 3600))
        )

    def tearDown(self):
        """Clean up"""

        pass

    def test_favicon(self):
        """Tests favicon endpoint"""

        result = self.client.get('/weather/v1/favicon.ico')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_dayhourforecast(self):
        """Tests the dayhourforecast endpoint"""

        good_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}/{self.date_time[1]}/'
        )

        good_response_c = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}'
            f'/{self.date_time[1]}/temperature/?temp_unit=c'
        )

        good_response_k = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}'
            f'/{self.date_time[1]}/?temp_unit=k'
        )

        bad_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}/2/'
        )

        self.assertEqual(good_response.status_code, 200)
        self.assertIn('description', json.loads(good_response.data))
        self.assertIn('temperature', json.loads(good_response.data))
        self.assertIn('pressure', json.loads(good_response.data))
        self.assertIn('humidity', json.loads(good_response.data))

        self.assertEqual(good_response_c.status_code, 200)
        self.assertIn('C', json.loads(good_response_c.data).get('temperature'))

        self.assertEqual(good_response_k.status_code, 200)
        self.assertIn('K', json.loads(good_response_k.data).get('temperature'))

        self.assertEqual(bad_response.status_code, 200)
        self.assertIn('error', str(bad_response.data))

    def test_dayhourdescriptionforecast(self):
        """Tests the dayhourdescriptionforecast endpoint"""

        good_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}'
            f'/{self.date_time[1]}/description/'
        )

        bad_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}/2/description/'
        )

        self.assertEqual(good_response.status_code, 200)
        self.assertIn('description', json.loads(good_response.data))

        self.assertEqual(bad_response.status_code, 200)
        self.assertIn('error', str(bad_response.data))

    def test_dayhourtemperatureforecast(self):
        """Tests the dayhourtemperatureforecast endpoint"""

        good_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}'
            f'/{self.date_time[1]}/temperature/'
        )

        good_response_c = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}'
            f'/{self.date_time[1]}/temperature/?temp_unit=c'
        )

        good_response_k = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}'
            f'/{self.date_time[1]}/temperature/?temp_unit=k'
        )

        bad_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}/2/temperature/'
        )

        self.assertEqual(good_response.status_code, 200)
        self.assertIn('temperature', json.loads(good_response_c.data))

        self.assertEqual(good_response_c.status_code, 200)
        self.assertIn('C', json.loads(good_response_c.data).get('temperature'))

        self.assertEqual(good_response_k.status_code, 200)
        self.assertIn('K', json.loads(good_response_k.data).get('temperature'))

        self.assertEqual(bad_response.status_code, 200)
        self.assertIn('error', str(bad_response.data))

    def test_dayhourpressureforecast(self):
        """Tests the dayhourpressureforecast endpoint"""

        good_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}'
            f'/{self.date_time[1]}/pressure/'
        )

        bad_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}/2/pressure/'
        )

        self.assertEqual(good_response.status_code, 200)
        self.assertIn('pressure', json.loads(good_response.data))

        self.assertEqual(bad_response.status_code, 200)
        self.assertIn('error', str(bad_response.data))

    def test_dayhourhumidityforecast(self):
        """Tests the dayhourhumidityforecast endpoint"""

        good_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}'
            f'/{self.date_time[1]}/humidity/'
        )

        bad_response = self.client.get(
            f'/weather/v1/london/{self.date_time[0]}/2/humidity/'
        )

        self.assertEqual(good_response.status_code, 200)
        self.assertIn('humidity', json.loads(good_response.data))

        self.assertEqual(bad_response.status_code, 200)
        self.assertIn('error', str(bad_response.data))


if __name__ == "__main__":
    unittest.main()
