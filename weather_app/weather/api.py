# -*- coding: utf-8 -*-

"""
.. module:: .api.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os

from flask import Blueprint, send_from_directory
from flask_restful import Resource, Api
from webargs import fields, validate
from webargs.flaskparser import use_kwargs

from weather.utils import get_forecast


weather = Blueprint(
    "weather",
    __name__,
    url_prefix="/weather/v1",  # API version
    static_folder="static"
)

api = Api(weather, catch_all_404s=True)

temperature_args = {
    'temp_unit': fields.Str(
        required=False,
        validate=validate.OneOf(['c', 'C', 'k', 'K']),
        missing='c'
    ),
}


class Favicon(Resource):
    """
    Serves the favicon
    """

    def get(self):
        """
        Get method to serve the favicon

        Parameters
        ----------

        Returns
        -------
        dict
            Response object (file)
        """
        return send_from_directory(
            os.path.join(weather.root_path, "static"),
            "favicon.ico",
            mimetype="image/vnd.microsoft.icon"
        )


class DayHourForecast(Resource):
    """Endpoint for forecast"""

    @use_kwargs(temperature_args)
    def get(self, date, hourminute, temp_unit):
        """
        Get method for weather forecast per 3 hour interval.
        The daily intervals are: 00:00:00, 03:00:00, 06:00:00, 09:00:00,
        12:00:00, 15:00:00, 18:00:00, 21:00:00.
        The `hourminute` parameter will fetch the forecast data from the
        closest interval e.g. if `hourminute` is 1212 the data that will be
        returned will fall into the 12:00:00-15:00:00 time interval.)

        Parameters
        ----------
        date : str
            string in the format of `20180421`
        hourminute : str
            string in the format of `2100`
        temp_unit : str
            Returns temperature in Celsius or Kelvin.
            Has a default value of `c`.
            Values permitted: `c`, `C`, `k`, `K`

        Returns
        -------
        dict
            Forecast in json
        """
        return get_forecast(date, hourminute, temp_unit=temp_unit)


class DayHourDescriptionForecast(Resource):
    """Endpoint for forecast description"""

    def get(self, date, hourminute):
        """
        Get method for forecast part (weather description)

        Parameters
        ----------
        date : str
            string in the format of `20180421`
        hourminute : str
            string in the format of `2100`

        Returns
        -------
        dict
            Forecast in json
        """
        return get_forecast(date, hourminute, description=None)


class DayHourTemperatureForecast(Resource):
    """Endpoint for forecast temperature"""

    @use_kwargs(temperature_args)
    def get(self, date, hourminute, temp_unit):
        """
        Get method for forecast part (temperature)

        Parameters
        ----------
        date : str
            string in the format of `20180421`
        hourminute : str
            string in the format of `2100`
        temp_unit : str
            Returns temperature in Celsius or Kelvin.
            Has a default value of `c`.
            Values permitted: `c`, `C`, `k`, `K`

        Returns
        -------
        dict
            Forecast in json
        """
        return get_forecast(
            date, hourminute, temperature=None, temp_unit=temp_unit
        )


class DayHourPressureForecast(Resource):
    """Endpoint for forecast pressure"""

    def get(self, date, hourminute):
        """
        Get method for forecast part (pressure)

        Parameters
        ----------
        date : str
            string in the format of `20180421`
        hourminute : str
            string in the format of `2100`

        Returns
        -------
        dict
            Forecast in json
        """
        return get_forecast(date, hourminute, pressure=None)


class DayHourHumidityForecast(Resource):
    """Endpoint for forecast humidity"""

    def get(self, date, hourminute):
        """
        Get method for forecast part (humidity)

        Parameters
        ----------
        date : str
            string in the format of `20180421`
        hourminute : str
            string in the format of `2100`

        Returns
        -------
        dict
            Forecast in json
        """
        return get_forecast(date, hourminute, humidity=None)


api.add_resource(Favicon, "/favicon.ico", endpoint="favicon")

api.add_resource(
    DayHourForecast,
    "/london/<string:date>/<string:hourminute>/",
    endpoint="dayhourforecast"
)

api.add_resource(
    DayHourDescriptionForecast,
    "/london/<string:date>/<string:hourminute>/description/",
    endpoint="dayhourdescriptionforecast"
)

api.add_resource(
    DayHourTemperatureForecast,
    "/london/<string:date>/<string:hourminute>/temperature/",
    endpoint="dayhourtemperatureforecast"
)

api.add_resource(
    DayHourPressureForecast,
    "/london/<string:date>/<string:hourminute>/pressure/",
    endpoint="dayhourpressureforecast"
)

api.add_resource(
    DayHourHumidityForecast,
    "/london/<string:date>/<string:hourminute>/humidity/",
    endpoint="dayhourhumidityforecast"
)
